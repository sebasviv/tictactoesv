**INDICACIONES PARA CORRECTO DESPLIEGUE DE LA APLICACION**

Para el correcto despliegue de esta aplicacion, se requiere tener instalado Node.js.

* Instalar todas las dependencias con yarn install

* Ejecutar con yarn start

---

## Notas

1. La aplicacion esta conectada a una base de datos en firebase, por consiguiente no se necesita instalar aplicaciones externas para back-end.
2. Hasta el momento la aplicacion funciona en LOCALHOST y en el hosting de firebase https://tic-tac-toe-aed45.web.app 
3. Hasta el momento no se puede jugar en dos navegadores, ya que se presentan bugs.


---

