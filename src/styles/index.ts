import styled from "styled-components";

export const Container = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  width: 100vw;
  height: 80vh;
  justify-content: center;
`;
export const Row = styled.div`
  display: flex;
  justify-content: center;
`;
export const Block = styled.div`
  align-items: center;
  border: solid 2px lightblue;
  display: flex;
  justify-content: center;
  height: 200px;
  width: 200px;
  cursor: pointer;
  transition: 0.3s;

  &:hover {
    background-color: lightgray;
  }
`;

export const Button = styled.button`
  width: 156px;
`;
