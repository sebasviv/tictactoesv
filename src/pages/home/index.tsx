import React from 'react';
import { useNavigate } from 'react-router-dom';
import { makeStyles } from '@mui/styles';
import { Button, Typography } from '@mui/material';
import NewGameDialog from 'components/NewGameDialog';
import { DATANEWGAME, DATALOADGAME } from 'types';
import LoadGameDialog from 'components/LoadGameDialog';

const useStyles = makeStyles({
  root: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    border: 0,
    borderRadius: 3,
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    color: 'white',
    width: '100vw',
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    overflow: 'hidden',
    fontFamily: 'cursive !important',

  },
  text: {
    fontSize: '7rem',
    margin: 0,
  },
  subText: {
    marginTop: 0,
    marginBottom: '20px !important',
    fontSize: '3rem',
  },

  button: {
    width: '150px',
    height: '50px',
    marginBottom: '20px !important',
  }
});

const Home = () => {

  const [isNewGameDialog, setIsNewGameDialog] = React.useState<boolean>(false);
  const [isLoadGameDialog, setIsLoadGameDialog] = React.useState<boolean>(false);
  const classes = useStyles()
  const history = useNavigate()

  // Funcion que trae la informacion del dialogo para cargar nueva partida
  // y redirige a la pantalla del juego, tambien setea el id del juego en la URL
  // que se pasara por parametro en la pantalla del juego
  const handleDataNewGame = (data: DATANEWGAME) => {
    localStorage.setItem('dataNewGame', JSON.stringify(data))
    history(`/game/${data.idGame}`)
  }

  // Funcion que trae la informacion del dialogo para cargar una partida
  // y redirige a la pantalla del juego
  const handleDataLoadGame = (data: DATALOADGAME) => {
    history(`/game/${data.id}`)
  }
  //Funcion que se encarga de abrir el dialogo cuando es un nuevo juego
  const handleClick = () => {
    setIsNewGameDialog(true)
  }
  //Funcion que se encarga de abrir el dialogo cuando se cargara un nuevo juego
  const handleClickLoad = () => {
    setIsLoadGameDialog(true)
  }

  //Pantalla del home
  return <>
    <div className={classes.root}>
      <h1 className={classes.text} >TicTacToe</h1>
      <h4 className={classes.subText} >By Sebastian Viveros</h4>
      <Button onClick={handleClick} className={classes.button} variant='contained'>Nueva Partida</Button>
      <Button onClick={handleClickLoad} className={classes.button} variant='contained'>Cargar Partida</Button>
    </div>
    <NewGameDialog open={isNewGameDialog} close={() => setIsNewGameDialog(false)} dataNewGame={handleDataNewGame} />
    <LoadGameDialog open={isLoadGameDialog} close={() => setIsLoadGameDialog(false)} dataLoadGame={handleDataLoadGame} />
  </>
};

export default Home