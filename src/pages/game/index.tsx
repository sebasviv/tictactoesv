
import { Box, Button } from '@mui/material';
import { checkWin } from 'helpers';
import useGame from 'hooks/use-game';
import SendInfo from 'hooks/use-mark';
import React from 'react';
import { useParams } from 'react-router';
import { Container, Row, Block } from "styles"
import { SYMBOL, BLOCK } from "../../types"
import { makeStyles } from '@mui/styles';
import { useNavigate } from 'react-router-dom';
import ScoreGame from 'components/ScoreGame';
import WinDialog from 'components/WinDialog'

//Estilos en componentes del juego
const useStyles = makeStyles({
  root: {
    overflow: 'hidden',
    backgroundColor: '#4158D0',
    backgroundImage: 'linear-gradient(43deg, #4158D0 0%, #C850C0 46%, #FFCC70 100%)',
    width: '100vw',
    height: '100vh',

  },
  boxCustom: {
    display: 'flex',
    flexDirection: 'column',
    fontSize: '500%',
    fontFamily: 'cursive',
    color: 'papayawhip',
  },
  textTitle: {
    fontFamily: 'cursive',
    fontSize: '240%',
    marginBottom: 20,
    color: 'white',
    textShadow: '5px 5px 5px hsl(0deg 0% 13% / 53%)',
  },
  buttonHome: {
    margin: '20px !important',
  },
  resetButton: {
    margin: '20px !important',
  }
});

//Valores iniciales con los que empieza el juego
const initialValues: BLOCK[] = [
  ' ',
  ' ',
  ' ',
  ' ',
  ' ',
  ' ',
  ' ',
  ' ',
  ' ',
]

type PlayerNames = {
  player1: string,
  player2: string,
}

type PlayerScores = {
  score1: number,
  score2: number,
}

const Game = () => {
  const classes = useStyles()
  const history = useNavigate()
  const { id } = useParams()
  const { isFetching, game } = useGame(id ? id : '')
  const [startingTurn, setStartingTurn] = React.useState<string>('jugador1')
  const [board, setBoard] = React.useState<BLOCK[]>(initialValues)
  const [isXTurn, setIsXTurn] = React.useState<Boolean>(true)
  const [turnNumber, setTurnNumber] = React.useState<number>(1)
  const [message, setMessage] = React.useState<string>(`Es el turno de: ${startingTurn}`)
  const [winner, setWinner] = React.useState<Boolean>(false)
  const [winMessage, setWinMessage] = React.useState<string>('');
  const [reset, setReset] = React.useState<Boolean>(false);
  const [winDialog, setWinDialog] = React.useState<boolean>(false);
  const [playerNames, setPlayerNames] = React.useState<PlayerNames>({
    player1: 'jugador1',
    player2: 'jugador2',
  });

  const [score, setScore] = React.useState<PlayerScores>({
    score1: 0,
    score2: 0
  });

  //Hook encargado de traer la informacion guardada en cada sala de juego en Firebase
  React.useEffect(() => {
    console.log('esta leyendo', { isFetching, game });
    if (game) {
      if (game.board) {
        setBoard(game.board)
        setMessage(game.message)
        setTurnNumber(game.turnNumber)
        setWinner(game.winner)
        // setPlayerNames({player1: game.playerOne, player2:game.playerTwo})
        // setScore({score1: game.scoreOne, score2:game.scoreTwo})
      }
    }
  }, [game])

  //Hook encargado del Score del juego y cuando se gana
  React.useEffect(() => {
    if (winner === true) {
      setWinMessage(`${!isXTurn ? playerNames.player1 : playerNames.player2}`)
      setScore({
        score1: !isXTurn ? score.score1 + 1 : score.score1,
        score2: isXTurn ? score.score2 + 1 : score.score2,
      })
      setWinDialog(true)
      // setWinner(false)
    }
  }, [winner])

  //Hook encargado de la accion cuando es empate
  React.useEffect(() => {
    if (winner === false && turnNumber === 10) {
      setWinMessage('empate!')
    }
  }, [turnNumber])

  //Hook encargado de enviar informacion a la base de datos en Firebase
  React.useEffect(() => {
    if (turnNumber !== 1 || reset === true) {
      SendInfo(id ? id : '',
        {
          board: board,
          message: message,
          playerTurn: `${isXTurn ? playerNames.player1 : playerNames.player2}`,
          turnNumber: turnNumber,
          winner: winner,
          playerOne: playerNames.player1,
          playerTwo: playerNames.player2,
          scoreOne: score.score1,
          scoreTwo: score.score2
        }
      )
      if (reset === true) {
        setReset(false)
      }
    }

  }, [message, board, isXTurn, turnNumber, winner, score])

  //Hook encargado de setear los nombres de los jugadores desde localstorage
  React.useEffect(() => {
    let localObj = decodeLocalStorage()
    setPlayerNames({
      player1: localObj.player1,
      player2: localObj.player2
    })
  }, []);

  //Hook encargado de definir el jugador que empieza
  React.useEffect(() => {
    setStartingTurn(playerNames.player1)
  }, [playerNames]);

//Funcion encargada de la jugabilidad
//checkWin: Funcion encargada de comprobar laterales del puntero
  const handleClick = (index: number) => {
    console.log('board: ', board);
    if (board[index] === null || board[index] === '-' || board[index] === ' ') {
      const newBoard = { ...board }
      newBoard[index] = isXTurn ? 'x' : 'o'
      //check
      if (turnNumber >= 5) {
        if (isXTurn) {
          setWinner(checkWin(newBoard, 'x'))
        } else {
          setWinner(checkWin(newBoard, 'o'))
        }
      }
      setIsXTurn(!isXTurn)
      setMessage(`Es el turno de: ${!isXTurn ? playerNames.player1 : playerNames.player2}`)
      setTurnNumber(turnNumber + 1)
      setBoard(newBoard)
    } else {
      alert("error")
    }
  }
//Funcion encargada de resetear a valores iniciales el tablero, turnos y mensajes
  const handleReset = () => {
    setReset(true)
    setWinner(false)
    setTurnNumber(1)
    setStartingTurn(startingTurn === playerNames.player1 ? playerNames.player2 : playerNames.player1)
    setIsXTurn(startingTurn === playerNames.player1)
    setBoard(initialValues)
    setMessage(`Es el turno de: ${startingTurn}`)
    setWinMessage('')
    setWinDialog(false)
  }

  //Funcion encargada de volver a la pestaña de home
  const handleBackHome = () => {
    history(`/`)
  }

  //Funcion encargada de traer los nombres de los jugadores desde localstorage
  const decodeLocalStorage = () => {
    let local = localStorage.getItem('dataNewGame')
    if (local) {
      return JSON.parse(local)
    } else {
      return {
        player1: playerNames.player1,
        player2: playerNames.player2
      }
    }
  }
  return (
    <div className={classes.root}>
      <div style={{ height: '10%' }}>
        <Button className={classes.buttonHome} onClick={handleBackHome} variant='contained'>Volver al Home</Button>
      </div>
      <Container>
        <p className={classes.textTitle}>{message}</p>
        <Box className={classes.boxCustom}>
          <Row>
            <Block onClick={() => handleClick(0)}>{board[0] && board[0]}</Block>
            <Block onClick={() => handleClick(1)}>{board[1] && board[1]}</Block>
            <Block onClick={() => handleClick(2)}>{board[2] && board[2]}</Block>
          </Row>
          <Row>
            <Block onClick={() => handleClick(3)}>{board[3] && board[3]}</Block>
            <Block onClick={() => handleClick(4)}>{board[4] && board[4]}</Block>
            <Block onClick={() => handleClick(5)}>{board[5] && board[5]}</Block>
          </Row>
          <Row>
            <Block onClick={() => handleClick(6)}>{board[6] && board[6]}</Block>
            <Block onClick={() => handleClick(7)}>{board[7] && board[7]}</Block>
            <Block onClick={() => handleClick(8)}>{board[8] && board[8]}</Block>
          </Row>
        </Box>
        {/* <Button variant='contained' className={classes.resetButton} onClick={handleReset}>Restablecer tablero</Button> */}
        <p>{winMessage}</p>
      </Container>
      <ScoreGame
        nameOne={playerNames.player1}
        nameTwo={playerNames.player2}
        scoreOne={score.score1}
        scoreTwo={score.score2}
      />
      <WinDialog
        open={winDialog}
        close={() => setWinDialog(false)}
        result={winMessage}
        resetBoard={handleReset}
        finishGame={handleBackHome} />
    </div>
  )
}



export default Game;
