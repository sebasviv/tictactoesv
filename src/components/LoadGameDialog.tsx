import React from 'react';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import { Button } from '@mui/material';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import { makeStyles } from '@mui/styles';
import { DATALOADGAME } from 'types';

const useStyles = makeStyles({
    content: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    customInput: {
        marginBottom: '20px !important',
        width: '100%',
    },
    button: {
        width: '150px',
        height: '50px',
      }

});
interface LoadGameDialogProps {
    open: boolean;
    close: any;
    dataLoadGame: any;
}

const LoadGameDialog = ({ open, close, dataLoadGame }: LoadGameDialogProps) => {
    const [maxWidth, setMaxWidth] = React.useState<DialogProps['maxWidth']>('sm');
    const [fullWidth, setFullWidth] = React.useState(true);
    const classes = useStyles()

    const handleSubmit = (e: any) => {
        e.preventDefault()
        let loadData: DATALOADGAME ={
            id: e.target.idgame.value
        }
        dataLoadGame(loadData)
    }

    return (
        <Dialog
            open={open}
            onClose={close}
            fullWidth={fullWidth}
            maxWidth={maxWidth}
        >
            <DialogTitle>Digite los siguientes campos</DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit} className={classes.content}>
                    <TextField className={classes.customInput} id="filled-basic" label="ID de Sala de juego" variant="filled" name={'idgame'}/>
                    <Button type='submit' className={classes.button} variant='contained'>IR AL JUEGO!</Button>
                </form>

            </DialogContent>
        </Dialog>
    );
};

export default LoadGameDialog
