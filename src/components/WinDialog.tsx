import React from 'react';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import { Button } from '@mui/material';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import { makeStyles } from '@mui/styles';
import { DATALOADGAME } from 'types';

const useStyles = makeStyles({
    content: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        textTransform: 'uppercase',
    },
    button: {
        width: '150px',
        height: '50px',
        margin: '20px !important',
    },
    textContainer: {

    },
    buttonsContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }


});
interface WinDialogProps {
    open: boolean;
    close: any;
    result: any;
    resetBoard: any;
    finishGame: any;
}

const WinDialog = ({ open, close, resetBoard, finishGame, result }: WinDialogProps) => {
    const [maxWidth, setMaxWidth] = React.useState<DialogProps['maxWidth']>('sm');
    const [fullWidth, setFullWidth] = React.useState(true);
    const classes = useStyles()


    return (
        <Dialog
            open={open}
            onClose={close}
            fullWidth={fullWidth}
            maxWidth={maxWidth}
        >
            <DialogContent>
                <div className={classes.content}>
                <div>
                    <p>¡FELICIDADES {result} HAS GANADO ESTA PARTIDA!</p>
                </div>
                <div className={classes.buttonsContainer}>
                    <Button className={classes.button} onClick={resetBoard} variant='contained'>Volver a jugar!</Button>
                    <Button className={classes.button} onClick={finishGame} variant='contained'>Terminar juego!</Button>
                </div>
                </div>
            </DialogContent>
        </Dialog>
    );
};

export default WinDialog
