import React from 'react';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
    root: {

        backgroundImage: 'linear-gradient(62deg, #FBAB7E 0%, #F7CE68 100%)',
        width: '21%',
        height: '11%',
        display: 'flex',
        position: 'absolute',
        top: 0,
        right: 0,
        margin: 20,
        borderRadius: 10,
        justifyContent: 'center',
        flexDirection: 'column',
        padding: 10,
        textAlign: 'center',
        '& p': {
            margin: 0
        },
        textShadow: '5px 5px 5px hsl(0deg 0% 13% / 53%)',
        fontFamily: 'cursive',
        color: 'white',
        border: '1px solid white',

    },
    titleContainer: {
    },
    scoreContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    scoreBox: {

        padding: 5,
        width: '100%',
    }

});

interface ScoreGameProps {
    scoreOne: number
    scoreTwo: number
    nameOne: string
    nameTwo: string
}

const ScoreGame = ({ scoreOne, scoreTwo, nameOne, nameTwo }: ScoreGameProps) => {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <div className={classes.titleContainer}>
                <p>PUNTAJE</p>
            </div>
            <div className={classes.scoreContainer}>
                <div className={classes.scoreBox}>
                    <p>{nameOne}</p>
                    <p>{scoreOne}</p>
                </div>
                <div className={classes.scoreBox}>
                    <p>{nameTwo}</p>
                    <p>{scoreTwo}</p>
                </div>
            </div>
        </div>);
};

export default ScoreGame;
