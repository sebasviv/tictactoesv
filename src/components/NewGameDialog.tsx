import React from 'react';
import Dialog, { DialogProps } from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import { Button } from '@mui/material';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import { makeStyles } from '@mui/styles';
import { DATANEWGAME } from 'types';

const useStyles = makeStyles({
    content: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    customInput: {
        marginBottom: '20px !important',
        width: '100%',
    },
    button: {
        width: '150px',
        height: '50px',
      }

});
interface NewGameDialogProps {
    open: boolean;
    close: any;
    dataNewGame: any;
}

const NewGameDialog = ({ open, close, dataNewGame }: NewGameDialogProps) => {
    const [maxWidth, setMaxWidth] = React.useState<DialogProps['maxWidth']>('sm');
    const [fullWidth, setFullWidth] = React.useState(true);
    const classes = useStyles()

    const handleSubmit = (e: any) => {
        e.preventDefault()
        let newData: DATANEWGAME ={
            player1: e.target.player1.value,
            player2: e.target.player2.value,
            idGame: e.target.idGame.value
        }
        dataNewGame(newData)
    }

    return (
        <Dialog
            open={open}
            onClose={close}
            fullWidth={fullWidth}
            maxWidth={maxWidth}
        >
            <DialogTitle>Digite los siguientes campos</DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit} className={classes.content}>
                    <TextField className={classes.customInput} id="filled-basic" label="Nombre Jugador 1" variant="filled" name={'player1'}/>
                    <TextField className={classes.customInput} id="filled-basic" label="Nombre Jugador 2" variant="filled" name={'player2'}/>
                    <TextField className={classes.customInput} id="filled-basic" label="ID de la partida" variant="filled" name={'idGame'}/>
                    <Button type='submit' className={classes.button} variant='contained'>IR AL JUEGO!</Button>
                </form>

            </DialogContent>
        </Dialog>
    );
};

export default NewGameDialog
