import { db } from '../../services'
import { collection, addDoc } from "firebase/firestore";
import React from 'react';
import { IGame } from 'models';
import { doc, setDoc } from "firebase/firestore";

const SendInfo = async (id: string, info: IGame ) => {
try {
  const docRef = await setDoc(doc(db, 'game',id), info);
} catch (e) {
  console.error("Error adding document: ", e);
}
};


export default SendInfo
