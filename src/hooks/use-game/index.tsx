import React from 'react'
import { db } from '../../services'
import { BLOCK, SYMBOL } from '../../types'
import { collection, getDocs } from "firebase/firestore";
import { doc, onSnapshot } from "firebase/firestore";
import { IGame } from "../../models"


interface IOutput {
    isFetching: boolean
    game?: IGame
}


const useGame = (id: string): IOutput => {
    const [game, setGame] = React.useState<IGame | undefined>()
    const [isFetching, setIsFetching] = React.useState<boolean>(false);
   

    React.useEffect(() => {
        const unSubscribe = onSnapshot(doc(db, 'game', id), (doc) => {
            if (doc) {
                setGame(doc.data() as IGame)
            } else {
                console.log('No se encontro sala');
            }
        });

        return () => {
            unSubscribe()
        }
    }, [id]);


    return { isFetching, game }
}

export default useGame