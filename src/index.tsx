import React from 'react';
import ReactDOM from 'react-dom';
import RoutesFunction from 'routes';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom'
import './styles/global.css'

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <RoutesFunction/>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
