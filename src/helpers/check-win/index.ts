import { SYMBOL, BLOCK } from "../../types";

const checkWin = (board: BLOCK[], symbol: SYMBOL) => {
  switch (true) {
    //Horizontales
    case (board[0] === symbol && board[1] === symbol && board[2] === symbol):
      return true;
    case (board[3] === symbol && board[4] === symbol && board[5] === symbol):
      return true;
    case (board[6] === symbol && board[7] === symbol && board[8] === symbol):
      return true;
    //Verticales
    case (board[0] === symbol && board[3] === symbol && board[6] === symbol):
      return true;
    case (board[1] === symbol && board[4] === symbol && board[7] === symbol):
      return true;
    case (board[2] === symbol && board[5] === symbol && board[8] === symbol):
    //Diagonales
      return true;
    case (board[0] === symbol && board[4] === symbol && board[8] === symbol):
      return true;
    case (board[2] === symbol && board[4] === symbol && board[6] === symbol):
      return true;
    default:
        return false
  }
};

export default checkWin;
