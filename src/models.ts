import { BLOCK } from './types'

export interface IGame {
    board?: BLOCK[]
    message: string
    playerTurn: string
    turnNumber: number
    winner: Boolean
    playerOne: string
    playerTwo: string
    scoreOne: number
    scoreTwo: number

}