export type SYMBOL = 'x' | 'o'
export type BLOCK = SYMBOL | '-' | ' ' 

export type DATANEWGAME = {
    player1: string
    player2: string
    idGame: string
}


export type DATALOADGAME = {
    id: string
}