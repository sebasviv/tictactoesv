import React, { Suspense } from 'react';
import { Route, Routes } from "react-router-dom";




const HomeLazy = React.lazy(() => import('../pages/home'))
const GameLazy = React.lazy(() => import('pages/game'))


const RoutesFunction = () => {
    return (
        <Routes>
                <Route path="/game/:id" element={<Suspense fallback={<h1>Loading Page...</h1>}><GameLazy></GameLazy></Suspense>} />
                <Route path="/" element={<Suspense fallback={<h1>Loading Page...</h1>}><HomeLazy></HomeLazy></Suspense>} />
        </Routes>
    )
}


export default RoutesFunction
